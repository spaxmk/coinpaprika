package com.spaxmk.coinpaprika.data.dto

import com.google.gson.annotations.SerializedName
import com.spaxmk.coinpaprika.domain.model.Coin

data class CoinDto(
    val id: String,
    @SerializedName("in_active")
    val is_active: Boolean,
    @SerializedName("in_new")
    val is_new: Boolean,
    val name: String,
    val rank: Int,
    val symbol: String,
    val type: String
)

fun CoinDto.toCoin(): Coin {
    return Coin(
        id = id,
        is_active = is_active,
        name = name,
        rank = rank,
        symbol = symbol,
        type = type
    )
}