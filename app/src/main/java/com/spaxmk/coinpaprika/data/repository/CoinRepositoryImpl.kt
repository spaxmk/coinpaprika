package com.spaxmk.coinpaprika.data.repository

import com.spaxmk.coinpaprika.data.dto.CoinDetailDto
import com.spaxmk.coinpaprika.data.dto.CoinDto
import com.spaxmk.coinpaprika.data.remote.CoinPaprikaApi
import com.spaxmk.coinpaprika.domain.repository.CoinRepository
import javax.inject.Inject

class CoinRepositoryImpl @Inject constructor(
    private val api: CoinPaprikaApi
): CoinRepository{
    override suspend fun getCoins(): List<CoinDto> {
        return api.getCoins()
    }

    override suspend fun getCoinById(coinId: String): CoinDetailDto {
        return api.getCoinById(coinId)
    }
}