package com.spaxmk.coinpaprika.data.dto

data class LinksExtended(
    val stats: Stats,
    val type: String,
    val url: String
)