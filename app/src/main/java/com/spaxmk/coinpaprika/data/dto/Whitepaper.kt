package com.spaxmk.coinpaprika.data.dto

data class Whitepaper(
    val link: String,
    val thumbnail: String
)