package com.spaxmk.coinpaprika.presentation.coin_list

import com.spaxmk.coinpaprika.domain.model.Coin

data class CoinListState(
    val isLoading: Boolean = false,
    val coins: List<Coin> = emptyList(),
    val error: String = ""
)
