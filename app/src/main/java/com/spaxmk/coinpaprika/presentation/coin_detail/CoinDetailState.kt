package com.spaxmk.coinpaprika.presentation.coin_detail

import com.spaxmk.coinpaprika.domain.model.Coin
import com.spaxmk.coinpaprika.domain.model.CoinDetail

data class CoinDetailState(
    val isLoading: Boolean = false,
    val coin: CoinDetail? = null,
    val error: String = ""
)
