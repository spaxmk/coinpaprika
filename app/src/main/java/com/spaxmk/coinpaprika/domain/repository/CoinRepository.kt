package com.spaxmk.coinpaprika.domain.repository

import com.spaxmk.coinpaprika.data.dto.CoinDetailDto
import com.spaxmk.coinpaprika.data.dto.CoinDto

interface CoinRepository {

    suspend fun getCoins(): List<CoinDto>

    suspend fun getCoinById(coinId: String): CoinDetailDto

}