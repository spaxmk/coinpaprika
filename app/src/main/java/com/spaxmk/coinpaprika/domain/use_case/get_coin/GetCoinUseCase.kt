package com.spaxmk.coinpaprika.domain.use_case.get_coin

import com.spaxmk.coinpaprika.common.Resource
import com.spaxmk.coinpaprika.data.dto.toCoin
import com.spaxmk.coinpaprika.data.dto.toCoinDetail
import com.spaxmk.coinpaprika.domain.model.Coin
import com.spaxmk.coinpaprika.domain.model.CoinDetail
import com.spaxmk.coinpaprika.domain.repository.CoinRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetCoinUseCase @Inject constructor(
    private val repository: CoinRepository
) {
    operator fun invoke(coinId: String): Flow<Resource<CoinDetail>> = flow {
        try {
            emit(Resource.Loading())
            val coin = repository.getCoinById(coinId).toCoinDetail()
            emit(Resource.Success(coin))
        }catch (ex: HttpException){
            emit(Resource.Error(ex.localizedMessage ?: "An unexpected errpr pccured"))
        }catch (e: IOException){
            emit((Resource.Error("Couldn`t reach server. Check your internet connection")))
        }
    }
}