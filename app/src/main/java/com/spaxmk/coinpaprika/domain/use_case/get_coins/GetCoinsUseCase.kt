package com.spaxmk.coinpaprika.domain.use_case.get_coins

import com.spaxmk.coinpaprika.common.Resource
import com.spaxmk.coinpaprika.data.dto.toCoin
import com.spaxmk.coinpaprika.domain.model.Coin
import com.spaxmk.coinpaprika.domain.repository.CoinRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetCoinsUseCase @Inject constructor(
    private val repository: CoinRepository
) {
    operator fun invoke(): Flow<Resource<List<Coin>>> = flow {
        try {
            emit(Resource.Loading())
            val coins = repository.getCoins().map { it.toCoin()}
            emit(Resource.Success(coins))
        }catch (ex: HttpException){
            emit(Resource.Error(ex.localizedMessage ?: "An unexpected errpr pccured"))
        }catch (e: IOException){
            emit((Resource.Error("Couldn`t reach server. Check your internet connection")))
        }
    }
}